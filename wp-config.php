<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '163');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Dc[PTXiStvb3VW:%j8dBCnNaJjVNJIn`?GSg!> :p}qQ~s}!g/C]+m0L<Bq78D:y');
define('SECURE_AUTH_KEY',  '~F|1ON<*aYKY;LlI%jP8|r@o0|LZ_ nHBi[ry9x$O-%(:X7b9W+*zr[DXqSr>)&|');
define('LOGGED_IN_KEY',    'DHnjc]=1pV2S_xuU6quZYO^/W%y.m1q{PS3N!Go8*JKH5Ivbo&pMo_33M %rKe>/');
define('NONCE_KEY',        'P~Fx9SU={2JWZ_2hr.xg}n9*{2,/Ju$)kmzNl_@uYetc_u[ns~}*GS@cj e{]!P!');
define('AUTH_SALT',        '8][:@+?/1GZdfpr5]414~ma~B8x*$r<&*TMA`wD%t39]Ertq|,WElV?YAFye-Y`I');
define('SECURE_AUTH_SALT', 'dP1/C{&%Xy,V<=Pv>I-J)iu`uIRvJ,:mN5B8CHEOv{PJ7p~&w_.aQ|&%mudbV>wn');
define('LOGGED_IN_SALT',   '~7%A3(Tsj)4NW=4&*lKTBMurm&#V890e%e:Ki75XZPVBS3EXO`%`:+BG~V#6?[Vi');
define('NONCE_SALT',       '`NdBlX1#<!NY]u...+oWd:gkGK[0UmUt8Nro*Ec@3ZR_407D{sD6tw!K0mE58&wT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
