<?php get_header();/*Template Name: Offers*/ ?>
<div class="content">
	<div class="container">
        <div class="offers">
            <div class="top_image cover" style="background-image: url('<?php the_field('top_image');?>')"></div>
        </div>
        <div class="shares">
            <div id="share">SHARE</div>
            <input type="checkbox" id="socialCheckbox" />
            <div class="share-container">

                <a class="social-button twitter" href="https://twitter.com/home?status=http%3A//<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>"></a>


                <a class="social-button facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>"></a>

                <a class="social-button instagram"></a>
                <a class="social-button linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>">
                </a>
                <a class="social-button youtube"></a>
                <label for="socialCheckbox" class="social-check-label"></label>
            </div>
            <div class="expander"></div>
        </div>
        <div class="socials alc">
			<?php if(get_field('twitter','options')) { ?>
                <a href="<?php the_field('twitter','options'); ?>" class="i-c-tw"></a>
			<?php } ?>
			<?php if(get_field('instagram','options')) { ?>
                <a href="<?php the_field('instagram','options'); ?>" class="i-c-inst"></a>
			<?php } ?>
			<?php if(get_field('facebook','options')) { ?>
                <a href="<?php the_field('facebook','options'); ?>" class="i-c-fb"></a>
			<?php } ?>
			<?php if(get_field('linkedin','options')) { ?>
                <a href="<?php the_field('linkedin','options'); ?>" class="i-c-in"></a>
			<?php } ?>
			<?php if(get_field('youtube','options')) { ?>
                <a href="<?php the_field('youtube','options'); ?>" class="i-c-yb"></a>
			<?php } ?>
        </div>
    </div>

            <div class="container">
                <div class="offers">
            <div class="offers_block">
                <h2><?php the_field('top_title'); ?></h2>
                <h3><?php the_field('top_subtitle'); ?></h3>
                <?php if($week_offers = get_field('week_offers')) { ?>
                    <div class="week_offers flex">
                        <?php foreach($week_offers as $week_offer) { ?>
                            <div class="item flex">
                                <div class="info">
	                                <div class="day"><?php echo $week_offer['day']; ?></div>
	                                <?php echo $week_offer['offer']; ?>
                                </div>
                                <div class="image cover" style="background-image: url('<?php echo $week_offer['image']; ?>')"></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <div class="bottom">
                    <h3><?php the_field('bottom_offers_title'); ?></h3>
	                <?php if($bottom_offers = get_field('bottom_offers')) { ?>
                        <div class="bottom_offers">
			                <?php foreach($bottom_offers as $bottom_offer) { ?>
                                <div class="item flex">
                                    <div class="info">
                                        <div class="day"><?php echo $bottom_offer['title']; ?></div>
						                <?php echo $bottom_offer['info']; ?>
                                    </div>
                                    <div class="image cover" style="background-image: url('<?php echo $bottom_offer['image']; ?>')"></div>
                                </div>
			                <?php } ?>
                        </div>
	                <?php } ?>
                </div>
            </div>
        </div>
	</div>
</div>
<?php get_footer(); ?>

<?php if($wine_type = get_field('wine_type')) { ?>
    <div class="wine_type">
        <div class="swiper-container flex">
            <div class="swiper-wrapper">
				<?php foreach ($wine_type as $wt) { ?>
                    <div class="swiper-slide cover" style="background-image: url('<?php echo $wt['image'];?>');" data-title="<?php echo $wt['name'] ?>">
                    </div>
				<?php } ?>
            </div>
        </div>
        <div class="pagination_wrap">
            <div class="swiper-pagination"></div>
    </div>
<?php } ?>

<script>
    var mainSlider = new Swiper('.wine_type .swiper-container', {
        pagination: '.wine_type .swiper-container .swiper-pagination',
        clickable: true,
        paginationBulletRender: function (wine_type, index, className) {
            var slide = $('.wine_type .swiper-container').find('.swiper-slide')[index];
            return '<div class="' + className + '">' + $(slide).attr('data-title') + '</div>';
        }
    });
</script>