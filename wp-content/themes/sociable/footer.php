</div>
<footer>
   <div class="container">
        <div class="info">
            © 2018 | Privacy Policy | All Rights Reserved | <a class="mylink" target="_blank" rel="nofollow" href="https://lionwood.software">CREATED BY LIONWOOD SOFTWARE</a>
        </div>
   </div>
</footer>
<div class="fancybox-hidden">
    <div id="contact">
        <div class="contact_top">
            <a class="logo" href="<?php echo get_option('home') ?>">
                <img src="/wp-content/themes/sociable/images/logo2.png" alt="logo">
            </a>
            <div class="socials alc">
                <a href="#" class="i-c-tw"></a>
                <a href="#" class="i-c-inst"></a>
                <a href="#" class="i-c-fb"></a>
                <a href="#" class="i-c-in"></a>
                <a href="#" class="i-c-yb"></a>
            </div>
        </div>
        <div class="contact_info">
            <h2><?php the_field('contact_title','options'); ?></h2>
            <div class="info">
                <?php the_field('contact_info','options'); ?>
            </div>
            <div class="shadule">
                <h3><?php the_field('shadule_title','options'); ?></h3>
                <?php the_field('shadule_info','options'); ?>
            </div>
        </div>
        <div class="footer">
            <div class="info_bottom">
                © 2018 | Privacy Policy | All Rights Reserved | <a class="mylink" target="_blank" rel="nofollow" href="http://lionwood.software/">CREATED BY LIONWOOD SOFTWARE</a>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>