<?php get_header();/*Template Name: Food*/ ?>
    <div class="content">
        <div class="food">


			<?php if ( $menu_item = get_field( 'menu_item' ) ) { ?>
                <div class="food_list">
                    <div class="food_list_slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                            <?php $i=1; ?>
                                <!--								<h1>--><?php //the_title(); ?><!--</h1>-->
								<?php foreach ( $menu_item as $price_l ) { ?>
                                    <div class="swiper-slide cover animatedParent"
                                         style="background-image: url('<?php echo $price_l['image']; ?>');" id="<?php echo $i++; ?>"
                                         data-title="<?php echo $price_l['title'] ?>">
                                        <div class="info animated fadeInUpShort">
                                            <h2><?php echo $price_l['title'] ?></h2>
                                            <div class="price_list_content">
												<?php foreach ( $menu_item as $price_l ) : ?>
													<?php if ( $menu_list = $price_l['menu_list'] ) { ?>
                                                        <div class="prices_content">
															<?php foreach ( $menu_list as $price_content ): ?>
                                                                <div class="wrap flex">
                                                                    <div class="name">
																		<?php echo $price_content['name']; ?>
                                                                    </div>
                                                                    <div class="price flex">
																		<div class="tobottom"><?php echo $price_content['price']; ?></div>
                                                                    </div>
                                                                </div>
															<?php endforeach; ?>
                                                        </div>
													<?php } ?>
												<?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
                            </div>
                        </div>
                        <div class="pagination_wrap">
                            <div class="help">
                                <div class="gotomenu">
                                    <a href="<?php echo get_option( 'home' ) ?>/food/">Go to menu</a>
                                </div>
                                <h3><?php the_title(); ?></h3>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
			<?php } ?>


        </div>
    </div>
<?php get_footer(); ?>