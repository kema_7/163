<?php
header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
header('Content-Type: text/html; charset=utf-8');
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header('X-UA-Compatible: IE=Edge');
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title><?php seo_title(); ?></title>
   <meta name="MobileOptimized" content="width"/>
   <meta name="HandheldFriendly" content="True"/>
   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
   <meta name="viewport" content="initial-scale=1.0, width=device-width">
   <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="main">
	<?php if( get_field('background_as_an_image','options') ): ?>
        <div class="background" style="background-image: url('<?php the_field('main_background','options');?>')"></div>
	<?php endif; ?>
	<?php if( get_field('background_as_color','options') ): ?>
         <div class="background" style="background-color: <?php the_field('color','options');?>"></div>
	<?php endif; ?>
   <header>
      <div class="container">
          <div class="header_content alc">
              <div class="logos">
                  <a class="logo" href="<?php echo get_option('home') ?>">
                      <img id="firstlogo" src="/wp-content/themes/sociable/images/logo.png" alt="logo">
                      <img id="secondlogo" src="/wp-content/themes/sociable/images/logo2.png" alt="logo">
                  </a>
              </div>
              <div id="menuOpen">
                  <button class="hamburger hamburger--collapse" type="button">
                      <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                      </span>
                  </button>
                  <div class="close">Close</div>
                  <div class="socials alc closed">
	                  <?php if(get_field('twitter','options')) { ?>
                          <a href="<?php the_field('twitter','options'); ?>" class="i-c-tw"></a>
	                  <?php } ?>
	                  <?php if(get_field('instagram','options')) { ?>
                          <a href="<?php the_field('instagram','options'); ?>" class="i-c-inst"></a>
	                  <?php } ?>
	                  <?php if(get_field('facebook','options')) { ?>
                          <a href="<?php the_field('facebook','options'); ?>" class="i-c-fb"></a>
	                  <?php } ?>
	                  <?php if(get_field('linkedin','options')) { ?>
                          <a href="<?php the_field('linkedin','options'); ?>" class="i-c-in"></a>
	                  <?php } ?>
	                  <?php if(get_field('youtube','options')) { ?>
                          <a href="<?php the_field('youtube','options'); ?>" class="i-c-yb"></a>
	                  <?php } ?>
                  </div>
                  <div class="info footer closed">
                      © 2018 | Privacy Policy | All Rights Reserved | <a class="mylink" target="_blank" rel="nofollow" href="http://lionwood.software/">CREATED BY LIONWOOD SOFTWARE</a>
                  </div>
              </div>
              <nav id="mainMenu">
                  <a class="logo2" href="<?php echo get_option('home') ?>">
                      <img src="/wp-content/themes/sociable/images/logo2.png" alt="logo">
                  </a>
		          <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location' => 'main_menu')); ?>
              </nav>
              <a href="<?php echo get_option('home') ?>/bookings/" class="mylink">Reserve</a>
          </div>
      </div>
   </header>







