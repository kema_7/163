<?php get_header();/*Template Name: Group*/ ?>
<div class="content">
	<div class="container">
        <div class="shares">
            <div id="share">SHARE</div>
            <input type="checkbox" id="socialCheckbox" />
            <div class="share-container">

                <a class="social-button twitter" href="https://twitter.com/home?status=http%3A//<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>"></a>


                <a class="social-button facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>"></a>

                <a class="social-button instagram"></a>
                <a class="social-button linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>">
                </a>
                <a class="social-button youtube"></a>
                <label for="socialCheckbox" class="social-check-label"></label>
            </div>
            <div class="expander"></div>
        </div>
        <div class="socials alc">
			<?php if(get_field('twitter','options')) { ?>
                <a href="<?php the_field('twitter','options'); ?>" class="i-c-tw"></a>
			<?php } ?>
			<?php if(get_field('instagram','options')) { ?>
                <a href="<?php the_field('instagram','options'); ?>" class="i-c-inst"></a>
			<?php } ?>
			<?php if(get_field('facebook','options')) { ?>
                <a href="<?php the_field('facebook','options'); ?>" class="i-c-fb"></a>
			<?php } ?>
			<?php if(get_field('linkedin','options')) { ?>
                <a href="<?php the_field('linkedin','options'); ?>" class="i-c-in"></a>
			<?php } ?>
			<?php if(get_field('youtube','options')) { ?>
                <a href="<?php the_field('youtube','options'); ?>" class="i-c-yb"></a>
			<?php } ?>
        </div>
		<div class="group">
			<div class="top_image cover" style="background-image: url('<?php the_field('top_image');?>')"></div>
        </div>
    </div>

			<div class="container">
                <div class="group">
                    <h1><?php the_title(); ?></h1>
                    <div class="description flex shadow">
                        <div class="text">
                            <h2><?php the_field('title'); ?></h2>
                            <div class="info">
				                <?php the_field('info'); ?>
                            </div>
                        </div>
                        <div class="image cover" style="background-image: url('<?php the_field('image');?>')"></div>
                    </div>
                </div>
            </div>

</div>
<?php get_footer(); ?>