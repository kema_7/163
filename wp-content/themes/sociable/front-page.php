<?php get_header(); ?>
   <div class="content">
      <div class="home_content">
          <div class="container">

              <div class="shares">
                  <div id="share">SHARE</div>
                  <input type="checkbox" id="socialCheckbox" />
                  <div class="share-container">

                      <a class="social-button twitter" href="https://twitter.com/home?status=http%3A//<?php $permalink = get_permalink();
			          $find = array( 'http://', 'https://' );
			          $replace = '';
			          $output = str_replace( $find, $replace, $permalink );
			          echo $output; ?>"></a>

                      <a class="social-button facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php $permalink = get_permalink();
			          $find = array( 'http://', 'https://' );
			          $replace = '';
			          $output = str_replace( $find, $replace, $permalink );
			          echo $output; ?>"></a>

                      <a class="social-button linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php $permalink = get_permalink();
			          $find = array( 'http://', 'https://' );
			          $replace = '';
			          $output = str_replace( $find, $replace, $permalink );
			          echo $output; ?>">
                      </a>
                      <label for="socialCheckbox" class="social-check-label"></label>
                  </div>
                  <div class="expander"></div>
              </div>
              <div class="socials alc">
		          <?php if(get_field('twitter','options')) { ?>
                      <a href="<?php the_field('twitter','options'); ?>" class="i-c-tw"></a>
		          <?php } ?>
		          <?php if(get_field('instagram','options')) { ?>
                      <a href="<?php the_field('instagram','options'); ?>" class="i-c-inst"></a>
		          <?php } ?>
		          <?php if(get_field('facebook','options')) { ?>
                      <a href="<?php the_field('facebook','options'); ?>" class="i-c-fb"></a>
		          <?php } ?>
		          <?php if(get_field('linkedin','options')) { ?>
                      <a href="<?php the_field('linkedin','options'); ?>" class="i-c-in"></a>
		          <?php } ?>
		          <?php if(get_field('youtube','options')) { ?>
                      <a href="<?php the_field('youtube','options'); ?>" class="i-c-yb"></a>
		          <?php } ?>
              </div>

		      <?php if($home_slider = get_field('home_slider')) { ?>
                  <div class="home_slider">
                      <div class="swiper-container">
                          <div class="swiper-wrapper">
                              <?php $i=2; ?>
						      <?php foreach ($home_slider as $slider) { ?>
                                  <div class="swiper-slide cover" style="background: url('<?php echo $slider['image'];?>') no-repeat; background-size: cover;"  data-title="0<?php echo $i++ ; ?>" >
                                        <div class="slider_info">
                                            <h3><?php echo $slider['main_title']; ?></h3>
                                            <div class="subtitle">
                                                <?php echo $slider['subtitle']; ?>
                                            </div>
                                            <div class="link">
                                                <a href="<?php echo $slider['link_address']; ?>"><?php echo $slider['link_name']; ?></a>
                                            </div>
                                        </div>
                                  </div>
						      <?php } ?>
                          </div>
                      </div>
                      <div class="swiper-pagination"></div>
                      <div class="swiper-button-next i-angle-right">Next</div>
                      <div class="swiper-button-prev i-angle-left">Prev</div>
                  </div>
		      <?php } ?>
          </div>
      </div>
   </div>
<?php get_footer(); ?>