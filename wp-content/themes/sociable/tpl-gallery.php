<?php get_header();/*Template Name: Gallery*/ ?>
<div class="content">
	<div class="container">
        <div class="shares">
            <div id="share">SHARE</div>
            <input type="checkbox" id="socialCheckbox" />
            <div class="share-container">

                <a class="social-button twitter" href="https://twitter.com/home?status=http%3A//<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>"></a>


                <a class="social-button facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>"></a>

                <a class="social-button instagram"></a>
                <a class="social-button linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php $permalink = get_permalink();
				$find = array( 'http://', 'https://' );
				$replace = '';
				$output = str_replace( $find, $replace, $permalink );
				echo $output; ?>">
                </a>
                <a class="social-button youtube"></a>
                <label for="socialCheckbox" class="social-check-label"></label>
            </div>
            <div class="expander"></div>
        </div>
        <div class="socials alc">
			<?php if(get_field('twitter','options')) { ?>
                <a href="<?php the_field('twitter','options'); ?>" class="i-c-tw"></a>
			<?php } ?>
			<?php if(get_field('instagram','options')) { ?>
                <a href="<?php the_field('instagram','options'); ?>" class="i-c-inst"></a>
			<?php } ?>
			<?php if(get_field('facebook','options')) { ?>
                <a href="<?php the_field('facebook','options'); ?>" class="i-c-fb"></a>
			<?php } ?>
			<?php if(get_field('linkedin','options')) { ?>
                <a href="<?php the_field('linkedin','options'); ?>" class="i-c-in"></a>
			<?php } ?>
			<?php if(get_field('youtube','options')) { ?>
                <a href="<?php the_field('youtube','options'); ?>" class="i-c-yb"></a>
			<?php } ?>
        </div>
        <div class="gallery_content">
            <div class="top_gallery cover" style="background-image: url('<?php the_field('top_image');?>')">

            </div>
        </div>
    </div>

                <div class="container">
                    <div class="gallery_content">
                        <div class="gallery_items">
		                    <?php if( have_rows('galleries') ): ?>
			                    <?php while ( have_rows('galleries') ) : the_row(); ?>
				                    <?php $images = get_sub_field('gallery');
				                    if( $images ): ?>
                                        <div class="items">
						                    <?php foreach( $images as $image ): ?>
                                                <div class="item">
                                                    <a class="fancy" href="<?php echo $image['url']; ?>">
                                                        <div class="image_single cover" style="background-image: url('<?php echo $image['sizes']['free']; ?>')">
                                                            <div class="bg_image" style="background-image: url('http://163.test/wp-content/uploads/2018/03/bg.png')">
                                                                <div class="square">
                                                                    <span>View</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
						                    <?php endforeach; ?>
                                        </div>
				                    <?php endif; ?>
			                    <?php endwhile; ?>
		                    <?php else : ?>
		                    <?php endif; ?>
                        </div>
                    </div>
                </div>
</div>
<?php get_footer(); ?>
