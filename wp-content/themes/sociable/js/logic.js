$ = jQuery;
/*Functions*/

$(document).ready(function () {
    "use strict";
    /*Init*/
    $("select").selbel();
    /*Init Sliders*/
    $(window).on('load', function () {

        var mainSlider = new Swiper('.wine_type .swiper-container', {
            pagination: '.wine_type .swiper-pagination',
            paginationClickable: true,
            effect: 'fade',
            autoHeight: true,
            slidesPerView: 1,
            fade: {crossFade: true},
            virtualTranslate: true,
            hashNav: true,
            history: '',
            paginationBulletRender: function (wine_type, index, className) {
                var slide = $('.wine_type .swiper-container').find('.swiper-slide')[index];
                return '<div class="' + className + '">' + $(slide).attr('data-title') + '</div>';
            }
        });


        var price_slider_bottom = new Swiper('.price_list_content .swiper-container', {
            effect: 'fade',
            autoHeight: true,
            roundLengths: true,
            fade: {crossFade: true},
            virtualTranslate: true,
            runCallbacksOnInit: true
        });

        var wines = new Swiper('.food .swiper-container', {
            direction: 'vertical',
            pagination: '.food .swiper-pagination',
            paginationBulletRender: function (food, index, className) {
                var sliderok = $('.food').find('.swiper-slide')[index];
                return '<div class="' + className + '">' + $(sliderok).attr('data-title') + '</div>';
            },
            paginationClickable: true,
            //autoHeight: true,
            spaceBetween: 0,
            slidesPerView: 1,
            fade: {crossFade: true}
            //mousewheelControl: true
        });


        var swiper = new Swiper('.home_slider .swiper-container', {
            loop:true,
            speed: 900,
            direction: 'horizontal',
            nextButton: '.home_slider .swiper-button-next',
            prevButton: '.home_slider .swiper-button-prev',
            slidesPerView: 1,
            paginationClickable: true,
            spaceBetween: 0,
            mousewheelControl: true,
            pagination: '.home_slider .swiper-pagination',
            paginationBulletRender: function (home_slider, index, className) {
                var slide = $('.home_slider').find('.swiper-slide')[index];
                return '<div class="' + className + '">' + $(slide).attr('data-title') + '</div>';
            }
        });
    });
});

$('.wpcf7-form-control-wrap').click(function() {
    $('span.wpcf7-not-valid-tip').css({
        'display': 'none'
    });
});
$('.wpcf7-response-output').click(function() {
    $(".wpcf7-response-output").hide()
});

$( "#menu-item-179 a" ).addClass( "fancy" );

$('.home_content .swiper-pagination-bullet').text("text");

$("a.fancy").fancybox({
    'transitionIn'	:	'elastic',
    'transitionOut'	:	'elastic',
    'speedIn'		:	600,
    'speedOut'		:	200,
    'overlayShow'	:	false,
    'showNavArrows' : true,
    toolbar: true,
    infobar: true
});

$( "#menu-item-179 a" ).addClass( "fancy" );


