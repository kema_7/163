<?php get_header(); ?>
<div class="e404">
    <div class="content">
        <div class="container">
            <a class="logo" href="<?php echo get_option('home') ?>">
                <img src="/wp-content/themes/sociable/images/logo.png" alt="logo">
            </a>
            <div class="not_found">
                <h2>Page not found</h2>
            </div>
            <div class="go_home">
                <a href="<?php echo get_option('home') ?>">HOME</a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

