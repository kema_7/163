	<div class="content food_navigation cover" style="background-image: url('<?php the_field('background_image');?>')">
		<?php get_header();/*Template Name: Food Nav*/ ?>
		<div class="container">
			<div class="foodnav_container">
                <div class="shares">
                    <div id="share">SHARE</div>
                    <input type="checkbox" id="socialCheckbox" />
                    <div class="share-container">

                        <a class="social-button twitter" href="https://twitter.com/home?status=http%3A//<?php $permalink = get_permalink();
						$find = array( 'http://', 'https://' );
						$replace = '';
						$output = str_replace( $find, $replace, $permalink );
						echo $output; ?>"></a>

                        <a class="social-button facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//<?php $permalink = get_permalink();
						$find = array( 'http://', 'https://' );
						$replace = '';
						$output = str_replace( $find, $replace, $permalink );
						echo $output; ?>"></a>

                        <a class="social-button linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php $permalink = get_permalink();
						$find = array( 'http://', 'https://' );
						$replace = '';
						$output = str_replace( $find, $replace, $permalink );
						echo $output; ?>">
                        </a>
                        <label for="socialCheckbox" class="social-check-label"></label>
                    </div>
                    <div class="expander"></div>
                </div>
                <div class="socials alc">
					<?php if(get_field('twitter','options')) { ?>
                        <a href="<?php the_field('twitter','options'); ?>" class="i-c-tw"></a>
					<?php } ?>
					<?php if(get_field('instagram','options')) { ?>
                        <a href="<?php the_field('instagram','options'); ?>" class="i-c-inst"></a>
					<?php } ?>
					<?php if(get_field('facebook','options')) { ?>
                        <a href="<?php the_field('facebook','options'); ?>" class="i-c-fb"></a>
					<?php } ?>
					<?php if(get_field('linkedin','options')) { ?>
                        <a href="<?php the_field('linkedin','options'); ?>" class="i-c-in"></a>
					<?php } ?>
					<?php if(get_field('youtube','options')) { ?>
                        <a href="<?php the_field('youtube','options'); ?>" class="i-c-yb"></a>
					<?php } ?>
                </div>
				<?php if($food_nav = get_field('food_nav')) { ?>
                    <div class="food_nav">
						<?php foreach($food_nav as $food) { ?>
                            <div class="item">
                                <a href="<?php echo $food['link']; ?>"><?php echo $food['name']; ?></a>
                            </div>
						<?php } ?>
                    </div>
				<?php } ?>
            </div>
		</div>
		<script>
            $ = jQuery;
            $('.content table').wrap('<div class="table_wrap">');
            $("#menuOpen button").on('click',function (e) {
                $(this).parent().toggleClass("opened");
                $(this).toggleClass('is-active');
            });
		</script>
	</div>